# Vantagens da Replicação:
1. Reduzir consumo de recurso do banco principal.
2. Ter um backup em tempo real do banco principal.
3. Recomendado para bancos que tenham um tanho muito grande e estejam consumindo muito tempo para realizar o processo por dump.
   
# Como ocorre a replicação:
1. O metodo apresentado é baseada em "WAL" Write-Ahead Logging.
   1. Resumidamente todas as alterações realizadas no banco de dados, são gravadas em dados do tipo WAL e após consolidadas no banco de dados, são escritas no disco.

# Para o Labaratório serão utilizados servidores Docker.

# Passo 1:

-  Criar um usuário "recplicador" que ficará monitorando o processo de WAL e relizando a replicação.
-  Utilizar a [role](./user-role.sql).
-  Alterar o pg_hba.conf ajustando a permissão do usuário.
      ```
      host    replication     all             0.0.0.0/32              scram-sha-256 
      ```

# Passo 2:

- Criar "SLOT DE REPLICAÇÃO"
  - É o espaço onde o Postgres vai armazenar os arquivos de WAL e fiquem disponíveis para as réplicas. Para que caso elas se desconectem, esses ficarão ali e as replicas poderam consumir esses dados e ajustar a sincronização. [Material Auxilizar sobre Slots](https://hevodata.com/learn/postgresql-replication-slots/)
- Utilizar o [comando](create-physical-replication.sql).

# Passo 3 (SERVIDOR PRINCIPAL):
- Alterar o "postgresql.conf": 
- Habilitar Função WAL (WRITE-AHEAD LOG):
   ```
   wal_level = logical #minimal, replica, or logical.
   ```
- Compressão:
  - Pode gerar aumento de consumo de CPU, deve ser acompanhando. 
   ```
   wal_compression = on # enable compression of full-page writes
   ```
- Checkpoint: 
  - Se for uma base pequena alterar os valores, caso não manter os padrãoes.
- Replicação:
  - Numero máximo de envios (walsender):
  ```
  max_wal_senders = 10 # max number of walsender processes
  ```
  - Numero máximo de replicação de Slots (replication slots):
  ```
  max_replication_slots = 10 # max number od replication slots
  ```

# Passo 4 (SERVIDOR REPLICA):
- Comunicação com o banco "primário":
  - A senha pode estar por padrão no ".pgpass".
  ```
  primary_conninfo = 'host=principal port=5433 user=replicador password=123456'                   # connection string to sending server
  ```
- Nome do slot primario criado:
  ```
  primary_slot_name = 'slot_replication_principal'                 # replication slot on sending server
  ```
- Habilitar hot_standby, para desabilitar consuktas enquanto está em mode de recovery:
  ```
  hot_standby = on                       # "off" disallows queries during recovery
  ```
- Habilitar "hot_standby_feedback", para prevenir conflitos de consultas.
  ```
  hot_standby_feedback = true             # send info from standby to prevent
                                          # query conflicts
  ```
- Recarregar as configurações do Postgres. 
  - Se Docker:
    - docker-compose up -d --force-recreate
  - Se onpremises:
    - systemctl reload postgres-14
  
Passo 5 - (Subindo servidor replica)

#pg_basebackup -h principal -U replicador -D /var/lib/postgresql/data -v -P -X s -c fast